// Include gulp
var gulp = require('gulp');

// Include plugins
var sass           = require('gulp-sass');
var concat         = require('gulp-concat');
var uglify         = require('gulp-uglify');
var livereload     = require('gulp-livereload');
var connect        = require('gulp-connect');
var plumber        = require('gulp-plumber');
var postcss        = require('gulp-postcss');
var autoprefixer   = require('autoprefixer');
var rename         = require('gulp-rename');
var replace        = require('gulp-replace');
var cssmin         = require('gulp-cssmin');
var concatCss      = require('gulp-concat-css');
var nunjucksRender = require('gulp-nunjucks-render');

// Live Reload
gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});

// JS files
gulp.task('js', function() {
    gulp.src('js/**/*.js')
        .pipe(livereload())
        .pipe(gulp.dest('./dist'));
});

// HTML files
gulp.task('html', function() {
    gulp.src('*.html')
        .pipe(livereload());
});

// Compile Sass
gulp.task('sass', function() {
    return gulp.src('src/sass/*.scss')
        .pipe(sass())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest('./'))
        .pipe(livereload());
});

// minify styles
gulp.task('min', function() {
    return gulp.src(['./style.css', './template.html'])
        .pipe(cssmin())
        .pipe(gulp.dest('./dist'));
});

// Compile nunjucks files
gulp.task('nunjucks', function() {
    // Gets .nunjucks files in pages
    return gulp.src('src/pages/*.nunjucks')
    // Renders template with nunjucks
    .pipe(nunjucksRender({
        path: ['src/templates/']
    }))
    // output files in root folder
    .pipe(gulp.dest('./'))
    .pipe(livereload());
});


// URL replace for Marketo

gulp.task('mktoreplace', function() {
    return gulp.src(['./style.css', './template.html'])
        .pipe(replace('./img/logos', 'http://resources.synnexcorp.com/rs/707-ZFS-030/images'))
        .pipe(replace('./img/icons', 'http://resources.synnexcorp.com/rs/707-ZFS-030/images'))
        .pipe(replace('./img/design', 'http://resources.synnexcorp.com/rs/707-ZFS-030/images'))
        .pipe(replace('url("./img/backgrounds', 'url(http://resources.synnexcorp.com/rs/707-ZFS-030/images'))
        .pipe(replace('href="./style.css"', 'href="http://resources.synnexcorp.com/rs/707-ZFS-030/images/style.css"'))
        .pipe(cssmin())
        .pipe(gulp.dest('./dist/mkto'));
});

// Watch files for changes


var filesToWatch = [
    'src/sass/**/*.scss',
    'src/pages/*.nunjucks',
    'src/templates/**/*.nunjucks',
    'js/**/*.js'
]

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(filesToWatch, ['nunjucks', 'sass', 'js'])
    // gulp.watch('css/**/*.scss', ['sass']);
    // gulp.watch('*.html', ['html', 'mktoreplace']);
    // gulp.watch('js/**/*.js', ['js']);
    // gulp.watch('./style.css', ['mktoreplace']);
});


// Default Task
gulp.task('default', ['nunjucks', 'sass', 'watch', 'connect']);
gulp.task('mkto', ['mktoreplace']);


